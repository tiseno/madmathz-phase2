//
//  FacebookFriendList.m
//  MadMath
//
//  Created by Jermin Bazazian on 9/11/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "FacebookFriendList.h"


@implementation FacebookFriendList

@synthesize tblfb = _tblfb;
@synthesize arr;
@synthesize matchedUser;
@synthesize userID;
@synthesize userName;
@synthesize userScore;
@synthesize imageData;

@synthesize searchview, searchbar, searchBar2, typeToSearchBar,tempString, reusableHeaderViews;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([[UIScreen mainScreen] bounds].size.height > 480)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_default_4in.png"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:
                                     [UIImage imageNamed:@"background_default.png"]];
    }
    
    isconnected = YES;
    
    // initialize and copy the original friendlist and existence friendlist
    beforesearch = [self.arr mutableCopy];
    matchedUserBeforeSearch = [self.matchedUser mutableCopy];
    
    typeToSearchBar = [[[UISearchBar alloc] initWithFrame:CGRectMake(10, 67, 270, 20)] retain];
    
    //remove searchbar default background
    [[typeToSearchBar.subviews objectAtIndex:0]removeFromSuperview];

    tempString = [[[NSString alloc] init] copy];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    countinterrupted = 0;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(refreshTable) userInfo:nil repeats:YES];
    
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
    [connectiontimer invalidate];
}

- (void)dealloc
{
    [_tblfb release];
    [searchBar2 release];
    [searchview release];
    [searchbar release];
    [typeToSearchBar release];
    [beforesearch release];
    [matchedUserBeforeSearch release];
    
    [super dealloc];
}

-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemovingScreen:self];
        countdown = 0;
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction LoadingScreen:self];
        isconnected = NO;
    }else if(![GlobalFunction NetworkStatus] && !isconnected){
        countdown++;
        if(countdown > 1200){
            LoginPage *GtestClasssViewController=[[[LoginPage alloc] initWithNibName:@"LoginPage"  bundle:nil] autorelease];
            [self presentModalViewController:GtestClasssViewController animated:YES];
        }
    }
    
    if(![GlobalFunction isplay]){
        ++countinterrupted;
        if(countinterrupted < 4)
            [GlobalFunction resumebgmusic];
        else if(countinterrupted == 4)
            [self.view makeToast:@"Background Music has been interrupted." duration:(0.5) position:@"center"];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return self.arr.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    //this 2 method is not suitable in this case... need some thinkering
    tableView.layer.borderWidth = 2.5;
    tableView.layer.cornerRadius = 10;
    tableView.layer.borderColor = [UIColor colorWithRed:0.3960 green:0.2627 blue:0.1294 alpha:1.0].CGColor;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell %d,  %d", indexPath.row, indexPath.section];
    //this is where each cell gets  uniquie identifier which prevents any problems
    
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.frame = CGRectMake(0, 0, 310, 55);
        
        //textLabel properties
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
        cell.textLabel.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
        cell.textLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cell.textLabel.clipsToBounds = YES;
        
        UILabel *lblTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 6, 115, 21)];
        lblTextLabel.font = [UIFont boldSystemFontOfSize:14];
        lblTextLabel.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
        lblTextLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        lblTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        lblTextLabel.clipsToBounds = YES;
        lblTextLabel.tag = 110;
        
        [cell.contentView addSubview:lblTextLabel];
        [lblTextLabel release];
        
        //detailTextLabel properties
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
        cell.detailTextLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
        cell.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cell.detailTextLabel.clipsToBounds = YES;
        
        //add line below
        CGRect lineFrame = CGRectMake(0, cell.frame.size.height+13, cell.frame.size.width, 2);
        UIView *line = [[UIView alloc] initWithFrame:lineFrame];
        line.layer.borderColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0].CGColor;
        line.layer.borderWidth = 2.5;
        [cell.contentView addSubview:line];
        [line release];
    }
    
    UILabel *lblTextLabel = (UILabel *)[cell.contentView viewWithTag:110];
    lblTextLabel.text = [[self.arr objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    cell.detailTextLabel.text = [self.matchedUser objectAtIndex:indexPath.row];

    // check whether to show the challenge or invite button
    if([[self.matchedUser objectAtIndex:indexPath.row] isEqualToString:@""])
    {
        //add cell image button
        UIImage *btnImage = [UIImage imageNamed:@"btn_invite.png"];
        UIButton *customButton = [[[UIButton alloc]init] autorelease];
        
        customButton.frame = CGRectMake(195.0f, 5.0f, 90.0f, 53.0f);
        customButton.backgroundColor = [UIColor clearColor];
        customButton.tag = indexPath.row;
        [customButton setBackgroundImage:btnImage forState:UIControlStateNormal];
        [cell addSubview:customButton];
        
        [customButton addTarget:self action:@selector(inviteFriendEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    else 
    {
        //add cell image button
        UIImage *btnImage = [UIImage imageNamed:@"btn_challenge.png"];
        UIButton *customButton = [[[UIButton alloc]init] autorelease];
        
        customButton.frame = CGRectMake(195.0f, 5.0f, 90.0f, 53.0f);
        customButton.backgroundColor = [UIColor clearColor];
        customButton.tag = indexPath.row;
        [customButton setBackgroundImage:btnImage forState:UIControlStateNormal];
        [cell addSubview:customButton];
        
        [customButton addTarget:self action:@selector(challenge:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    NSString *strurl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",
                        [[self.arr objectAtIndex:indexPath.row] objectForKey:@"id"]];
    
    NSURL *url = [NSURL URLWithString:strurl];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSData *imageDatas = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
                       
            cell.imageView.image = [UIImage imageWithData:imageDatas];
            cell.imageView.layer.masksToBounds = YES;
            cell.imageView.layer.cornerRadius = 5.0;
            cell.imageView.layer.borderColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0].CGColor;
            cell.imageView.layer.borderWidth = 1.5;
        });
    });
    
    return cell;
}

//method for adding header view into reuse-array
-  (void)registerHeaderView:(UITableViewCell *)headerView forReuseIdentifier:(NSString *)reuseIdentifier
{
    NSMutableDictionary *reusableDictionary  = [self reusableHeaderViews];
    
    if (reusableDictionary == nil)
    {
        reusableDictionary = [[NSMutableDictionary alloc] init];
        //creates a storage dictionary if one doesn’t exist
        
        self.reusableHeaderViews = reusableDictionary;
    }
    
    NSMutableArray *arrayForIdentifier = [[self reusableHeaderViews] objectForKey:reuseIdentifier];
    
    if (arrayForIdentifier == nil)
    {
        //creates an array to store views sharing a reuse identifier if one does not exist
        arrayForIdentifier = [[NSMutableArray alloc] init];
        
        [reusableDictionary setObject:arrayForIdentifier forKey:reuseIdentifier];
    }
    
    [arrayForIdentifier addObject:headerView];
}

//get header view in reuse-array
- (UITableViewCell *)reuseableHeaderForIdentifier:(NSString *)reuseIdentifier
{
    NSMutableArray *arrayOfViewsForIdentifier = [[self reusableHeaderViews]  objectForKey:reuseIdentifier];
    
    if (arrayOfViewsForIdentifier == nil)
    {
        return nil;  //We don’t have any of this kind!
    }
    
    NSInteger indexOfAvailableHeaderView = [arrayOfViewsForIdentifier indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop)
    {
            //If my view doesn’t have a superview, it’s not on-screen.
            return ![obj superview];
    }];
    
    if (indexOfAvailableHeaderView != NSNotFound)
    {
        UITableViewCell *headerView = [arrayOfViewsForIdentifier objectAtIndex:indexOfAvailableHeaderView];
        
        return headerView;    //Success!
    }
    
    return nil;
}
//
//

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
   /* UIView* headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 51)] autorelease];
    
    UIImageView *header = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bar_facebook_friend.png"]] autorelease];
    typeToSearchBar.delegate = self;
    
    [headerView addSubview:header];
    [headerView addSubview:typeToSearchBar];
    
    //self.searchview = [[UIView alloc] initWithFrame:CGRectMake(10, 22, 320, 44)];
    //[headerView addSubview:self.searchview];*/
    
    //return headerView;
    
    static NSString *headerReuseIdentifier = @"Header";
    
    UITableViewCell *cell = [self reuseableHeaderForIdentifier:headerReuseIdentifier];
    
    if(cell == nil)
    {
        //Didn’t locate a reusable
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:headerReuseIdentifier];
        
        UIView* headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 101)] autorelease];
        
        UIImageView *header = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bar_facebook_friend.png"]] autorelease];
        typeToSearchBar.delegate = self;
        
        [headerView addSubview:header];
        [headerView addSubview:typeToSearchBar];
        
        [cell addSubview:headerView];
    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 101.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72.0;
}

- (void)refreshTable
{    
    [_tblfb reloadData];
}

-(IBAction)back:(id)sender
{
    //[self.facebook logout:self];
    
    [super dismissModalViewControllerAnimated:YES];
}

-(IBAction)inviteFriendEvent: (id)sender
{
    if ([FBSession.activeSession isOpen])
    {
         //share on user's friend's wall
         
         NSString *strID = [[self.arr objectAtIndex:[sender tag]] objectForKey:@"id"];
         
         AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
         appDelegate.facebook = [[Facebook alloc] initWithAppId:[FBSession activeSession].appID andDelegate:nil];
         appDelegate.facebook.accessToken = [FBSession activeSession].accessToken;
         appDelegate.facebook.expirationDate = [FBSession activeSession].expirationDate;
         
         NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
         @"www.facebook.com/Madmathz", @"link",
         @"http://zrlim.com/mm/logo.png", @"picture",
         @"MadMathz", @"name",
         @"Game Descriptions", @"caption",
         @"This game will test your basic mathematic. You will be given a set of numbers and provided with answer then you need to find combination of number to match the answer within ONE minute. It is either you are loser or winner.", @"description",
         strID, @"to",
         nil];
         
         [appDelegate.facebook dialog:@"feed" andParams:params andDelegate:self];
                
       /* 
        
        //Send request !!!
        
        Facebook* facebook = [[Facebook alloc] initWithAppId:@"454580281249627" andDelegate:self];
    
        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"MadMathz Request", @"title",
                                   @"This is a testing message.",  @"message",
                                   strID, @"to",
                                   nil];
    
        [facebook dialog:@"apprequests" andParams:params andDelegate:self];*/
    }
    
    
    
   /* 
    
                                    //old page, deprecated.
    
        ShareToFaceBook1 *GtestClasssViewController=[[ShareToFaceBook1 alloc]
                        initWithNibName:@"ShareToFaceBook1"  bundle:nil];
    
        GtestClasssViewController.friendID = [[self.arr objectAtIndex:[sender tag]] objectForKey:@"id"];
    
        [self presentModalViewController:GtestClasssViewController animated:YES];
    
        [GtestClasssViewController release];
    
    */
}

-(IBAction)challenge: (id)sender
{
    // challenge mode
    
    ReadyScene *GtestClasssViewController=[[ReadyScene alloc] initWithNibName:@"ReadyScene"  bundle:nil];
    GtestClasssViewController.gamemode = 2;
    GtestClasssViewController.gameplayer = 1;
    GtestClasssViewController.userName = userName;
    GtestClasssViewController.userScore = userScore;
    GtestClasssViewController.userID = userID;
    GtestClasssViewController.imageData = imageData;
    GtestClasssViewController.splayerID = [[self.arr objectAtIndex:[sender tag]] objectForKey:@"id"];
    GtestClasssViewController.splayerUsername = [[self.arr objectAtIndex:[sender tag]] objectForKey:@"name"];
    GtestClasssViewController.levelMode = 0;
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
    [GtestClasssViewController release];
}

////////////
- (IBAction)cancelButtonAction:(id)sender
{
    [[self presentingViewController] dismissModalViewControllerAnimated:YES];
}

/* searchBar delegate methods */

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.typeToSearchBar resignFirstResponder];
    
    // restart the timer and connection timer
    
    // for the time being, close the refresh table timer
    //  timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(refreshTable) userInfo:nil repeats:YES];
    
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
    
    [self refreshTable];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        [self resetSearch];
        
        [self refreshTable];
        
        return;
    }

    tempString = searchText;
    
    [self filterContentForSearchText:searchText];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //stop the timer
    [timer invalidate];
    timer = nil;
    
    //stop the connection timer
    [connectiontimer invalidate];
    connectiontimer = nil;
}

-(void)resetSearch
{
    // reset both friendlist and matched friendlist to original
    self.arr = beforesearch;
    self.matchedUser = matchedUserBeforeSearch;
}

- (void)filterContentForSearchText:(NSString*)searchText
{
    categorylist = [self.arr mutableCopy];
	
    [categorylist removeAllObjects];
 
    // temporary matched friendlist
    tempMatchedUserList = [[NSMutableArray alloc] init];
    
	for (int i = 0; i < beforesearch.count;i++)
	{
        NSString *name = [[beforesearch objectAtIndex:i] objectForKey:@"name"];
        
        if ([name rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            continue;
        }
        else
        {
            [categorylist addObject:[beforesearch objectAtIndex:i]];
            
            // insert matched friend into temporary friendlist
            [tempMatchedUserList addObject:[matchedUserBeforeSearch objectAtIndex:i]];
        }
	}
    
    self.arr = categorylist;
    
    // changes matched user to temporary matched friend list
    self.matchedUser = tempMatchedUserList;
    
    [self.tblfb reloadData];
    
    [typeToSearchBar becomeFirstResponder];
}

//FBDialog delegate method
- (void) dialogCompleteWithUrl:(NSURL*) url
{
    if ([url.absoluteString rangeOfString:@"post_id="].location != NSNotFound)
    {
        //alert user of successful post
        NSLog(@"post : success");
    }
    else
    {
        //user pressed "cancel"
        NSLog(@"post : failed");
    }
}

@end
