//
//  BGSound.h
//  MadMath
//
//  Created by tiseno on 12/7/12.
//
//

#import <Foundation/Foundation.h>

@interface BGSound : NSObject
{
    
}

@property (nonatomic) float bgmusic;
@property (nonatomic) int soundstatus;

@end
