//
//  DatabaseAction.h
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/9/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQLite.h"
#import "BGSound.h"

@interface DatabaseAction : SQLite{
    BOOL runBatch;
}

@property (nonatomic)BOOL runBatch;

-(DataBaseInsertionResult)insertBGSound:(BGSound*)bgsound;

-(BGSound*)retrieveBGSound;

-(int)countRow;

-(DataBaseUpdateResult)updateBGMusic:(float)volume;

-(DataBaseUpdateResult)updateSound:(int)status;

@end
