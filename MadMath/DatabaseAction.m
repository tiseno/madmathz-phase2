//
//  DatabaseAction.m
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/9/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import "DatabaseAction.h"

@implementation DatabaseAction

@synthesize runBatch;

-(DataBaseInsertionResult)insertBGSound:(BGSound*)bgsound
{
    BOOL connectionIsOpenned=YES;
    
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    
    if(connectionIsOpenned)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into BGSound(BGMusic, Sound) values('%f', '%d');", bgsound.bgmusic, bgsound.soundstatus];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            
            return DataBaseInsertionSuccessful;
        }
        
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    
    return DataBaseInsertionFailed;
}

-(BGSound*)retrieveBGSound
{
    BGSound *bgsound= [[[BGSound alloc] init] autorelease];
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select BGMusic, Sound from BGSound"];
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            char* bgmusic = (char*)sqlite3_column_text(statement, 0);
            
            if(bgmusic)
            {
                NSString *music=[NSString stringWithUTF8String:(char*)bgmusic];
                bgsound.bgmusic = [music floatValue];
            }
            
            int soundstatus = sqlite3_column_int(statement, 1);
            bgsound.soundstatus = soundstatus;

        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return bgsound;
}

-(int)countRow
{
    int row = 0;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select COUNT(Sound) from BGSound"];
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            row = sqlite3_column_int(statement, 0);
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return row;
}

-(DataBaseUpdateResult)updateBGMusic:(float)volume
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString* updateSQL=[NSString stringWithFormat:@"update BGSound set BGMusic = %f", volume];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateFailed;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
}

-(DataBaseUpdateResult)updateSound:(int)status
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString* updateSQL=[NSString stringWithFormat:@"update BGSound set Sound = %d", status];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateFailed;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
}

@end
